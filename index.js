const epilogue = require('epilogue'),
	kauth = require('khem-auth'),
	orm = require('khem-sequelize-orm'),
	log = require('khem-log'),
	eMiddleImporter = require('./importer');

module.exports = {
	lib: epilogue,
	resources: [],

	checkIgnoreFiles: ignore => {
		if (process.env.EPILOGUE_MIDDLEWARE_IGNORE) {
			const ignoreFiles = process.env.EPILOGUE_MIDDLEWARE_IGNORE.split(',');
			for (let i = 0; i < ignoreFiles.length; i++) {
				ignore.push(ignoreFiles[i]);
			}
		}
		return ignore;
	},

	initialize(app, database, ignore) {
		const path = __dirname.replace('node_modules/khem-epilogue', '') + 'server/middlewares/epilogue/';
		ignore = this.checkIgnoreFiles(ignore || []);
		this.lib.initialize({ app: app, sequelize: database });
		// Import all models into epilogue
		this.resources = this.importRoutesFromModels(path, ignore);
	},

	/**
  * Create routes from sequelize models, for the model to be import epilogueService
  * needs the method epilogue() to return an object with epilogue format
  */
	importRoutesFromModels(path, ignore) {
		const models = orm.models;
		const resources = {};
		const middlewares = eMiddleImporter.getMiddlewares(path, ignore);
		const names = Object.keys(models);
		names.forEach(name => {
			let data = typeof models[name].epilogue === 'function' ? models[name].epilogue() : 'not_found';
			resources[name] = this.importRoute(data, name, middlewares, path, ignore);
		});
		return resources;
	},

	/**
  * Create a resource from an object
  
  * @param {object} data Object with epilogue format
  * @param {string} modelName
  * @param {array} [optional] middlewares
  */
	importRoute(data, modelName, middlewares, path, ignore) {
		if (data && data != 'not_found') {
			log.info('Importing epilogue resource: ' + modelName);
			data.model = orm.models[modelName];
			const resource = this.lib.resource(data);
			// Check if there is associations to recive
			if (orm.models[modelName].associations && data.includeMiddleware !== false) {
				log.info('Importing middleware (Associations) for resource: ' + modelName);
				resource.use(eMiddleImporter.get('Associations', path, ignore));
			}
			// Check if there is a middleware for the epilogue resource
			if (middlewares[modelName] || eMiddleImporter.get(modelName, path, ignore)) {
				const middleware = middlewares[modelName] || eMiddleImporter.get(modelName, path, ignore);
				log.info('Importing epilogue middleware: ' + modelName);
				resource.use(middleware);
			}
			if (data.authenticate) {
				log.info('Creating authentication: ' + modelName);
				this.loadAuthentication(resource, modelName, data.authenticate);
			}
			return resource;
		} else if (data == 'not_found') {
			log.warning('Epilogue object not found in model ' + modelName, 'khem-epilogue/index', 'importRoute');
		}
	},

	updateAssociations(request, instance, end) {
		if (instance.Model.associations) {
			let associations = [];
			const keys = Object.keys(instance.Model.associations);
			keys.forEach(key => associations.push(instance.Model.associations[key]));
			const promises = associations
				.filter(association => request[association.options.name.singular] || request[association.options.name.plural])
				.map(association => {
					const data = {
						model: association.options.name.singular,
						plural: association.options.name.plural,
						associations: request[association.options.name.singular] || request[association.options.name.plural],
						idName: orm.models[association.options.name.singular].primaryKeyField,
						accessors: association.accessors,
						instance: instance
					};
					const command = association.isMultiAssociation ? 'updateAssociationsNM' : 'updateAssociation1N';
					return orm[command](data);
				});
			Promise.all(promises)
				.then(() => end())
				.catch(err => {
					log.error(err, 'khem-epilogue', 'updateAssociations');
					return end();
				});
		} else {
			return end();
		}
	},

	loadAuthentication(resource, modelName, roles) {
		if (roles === true) {
			resource.create.auth(function(req, res, context) {
				return kauth.authenticateRoles(req, res, context.continue, modelName, 'create');
			});
			resource.update.auth(function(req, res, context) {
				return kauth.authenticateRoles(req, res, context.continue, modelName, 'update');
			});
			resource.delete.auth(function(req, res, context) {
				return kauth.authenticateRoles(req, res, context.continue, modelName, 'delete');
			});
			resource.list.auth(function(req, res, context) {
				return kauth.authenticateRoles(req, res, context.continue, modelName, 'list');
			});
			resource.read.auth(function(req, res, context) {
				return kauth.authenticateRoles(req, res, context.continue, modelName, 'read');
			});
		} else {
			resource.all.auth(function(req, res, context) {
				return kauth.authenticateUnited(req, res, context.continue, roles);
			});
		}
	}
};
