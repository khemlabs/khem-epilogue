'use strict';

const modelsNames = require('khem-sequelize-orm').modelsNames;

module.exports = {
	list: {
		start: {
			action: function(req, res, context) {
				const keys = Object.keys(req.query);
				keys.forEach(key => {
					if (modelsNames.indexOf(key) >= 0) {
						let index = this.include.findIndex(include => include.model.name === key);
						if (index >= 0) {
							this.include[index].where =
								typeof req.query[key] == 'string' ? JSON.parse(req.query[key]) : req.query[key];
						}
					}
				});
				return context.continue();
			}
		},
		complete: {
			action: function(req, res, context) {
				this.include.forEach(include => delete include.where);
				return context.continue();
			}
		}
	},
	create: {
		write: {
			after: function(req, res, context) {
				return require('khem-epilogue').updateAssociations(req.body, context.instance, context.continue);
			}
		}
	},

	update: {
		write: {
			after: function(req, res, context) {
				return require('khem-epilogue').updateAssociations(req.body, context.instance, context.continue);
			}
		}
	}
};
