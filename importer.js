const fs = require('fs'),
	log = require('khem-log');

/**
* eMiddlewareImporter imports all files with format [model].middleware.js
* witch are in the directory middlewares/epilogue/
*
* FILE FORMAT
* -----------
* Each file must respect the format of a middleware for "epilogue",
* this allows you to insert functionality, before, during and 
* after the ejecucción of "epilogue".
*
* IGNORE FILES
* ------------
* You can add files into the ignore list running the lift command with the environment variable
* EPILOGUE_MIDDLEWARE_IGNORE which receives a comma separated list of names
*
* For more information go to https://github.com/dchester/epilogue
*/
module.exports = {
	middlewares: {},
	filenames: [],
	middlewaresNAMES: [],

	isIgnoreFile(filename, ignore) {
		return ignore.indexOf(filename) >= 0;
	},

	validFileName(filename, ignore) {
		const name = filename.split('.');
		if (!this.isIgnoreFile(filename, ignore) && name.length == 3 && name[1] == 'middleware' && name[2] == 'js')
			return name[0];
		else {
			if (!this.isIgnoreFile(filename, ignore)) {
				log.warning(
					'The file ' + filename + ' has an invalid name for an epilogue middleware',
					'khem-epilogue/importer',
					'validFileName'
				);
			} else {
				log.info('Middleware importer: The file ' + filename + ' is in the ignore file array');
			}
			return false;
		}
	},

	checkFiles(path, ignore) {
		this.filenames = fs.readdirSync(path);
		// Load Associations middleware
		this.middlewares['Association'] = require('./Associations.middleware.js');
		this.middlewaresNAMES.push('Association');
		// Requirement of middlewares
		for (let i = 0; i < this.filenames.length; i++) {
			let filename = this.filenames[i];
			let name = this.validFileName(filename, ignore);
			if (name) {
				log.info('Fetching middleware file: ' + filename);
				try {
					this.middlewares[name] = require(path + filename);
					this.middlewaresNAMES.push(name);
				} catch (err) {
					log.error(err, 'khem-epilogue/importer', 'checkFiles');
				}
			}
		}
		return this.middlewares;
	},

	getMiddlewares(path, ignore) {
		log.info('Importing epilogue middlewares...');
		return this.middlewaresNAMES.length ? this.middlewares : this.checkFiles(path, ignore);
	},

	get(name, path, ignore) {
		if (this.middlewares[name]) return this.middlewares[name];
		if (!this.middlewaresNAMES.length) {
			const middlewares = this.checkFiles(path, ignore);
			return middlewares[name] || false;
		}
		return false;
	}
};
